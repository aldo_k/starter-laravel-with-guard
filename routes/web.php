<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Administrator\LogController;
use App\Http\Controllers\Administrator\RoleController;
use App\Http\Controllers\Administrator\ProductController;
use App\Http\Controllers\Administrator\ProfileController;
use App\Http\Middleware\AdminLoginMiddleware;

use App\Http\Controllers\Administrator\DashboardController;
use App\Http\Controllers\Administrator\UserAdminController;
use App\Http\Controllers\Administrator\PermissionController;
use App\Http\Controllers\Administrator\NotificationController;

use App\Http\Controllers\Administrator\AdminLogableController;
use App\Http\Controllers\Administrator\PublicStorageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('administrator.login');
});

Route::group([
    'prefix' => config('admin.prefix', 'administrator'),
    'as' => 'administrator.'
], function() {

    Route::group([
        'namespace' => 'App\Http\Controllers\Administrator',
    ], function () {
        Auth::routes(['register' => false]);
    });

    Route::group([
        'middleware' => [AdminLoginMiddleware::class],
    ], function () {
        Route::resource('/storage', PublicStorageController::class)->only(['index']);

        Route::resource('/', DashboardController::class)->only(['index']);
        Route::resource('/notification', NotificationController::class)->only(['index', 'show', 'store']);
        Route::resource('/profile', ProfileController::class)->only(['index', 'store']);

        Route::resource('/products', ProductController::class);

        Route::group(['as' => 'account.', 'prefix' => 'account'], function () {
            Route::resource('/admin', UserAdminController::class);
        });

        Route::group(['as' => 'system.', 'prefix' => 'system'], function () {
            Route::resource('/log', LogController::class)->only(['index']);
            Route::resource('/activity', AdminLogableController::class)->only(['index', 'destroy']);
        });

        Route::group(['as' => 'access.', 'prefix' => 'access'], function () {
            Route::resource('/role', RoleController::class);
            Route::resource('/permission', PermissionController::class)->only(['index', 'show', 'update']);
        });

    });
});