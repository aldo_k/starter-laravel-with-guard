<x-template-layout>
  <x-slot name="title">Create User</x-slot>

  <form action="{{ route('administrator.account.admin.store') }}" method="post">
    @csrf

    @include('administrator.user._partials._form', ['user' => app(config('admin.user', App\Models\User::class))])

    <div class="text-right">
      <button type="submit" class="btn btn-primary">
        Submit User
      </button>
    </div>
  </form>

</x-template-layout>