<?php

namespace App\View\Menus;

use Illuminate\View\Component;
use App\Helpers\Menu;

class Sidebar extends Component {

    public $menu;
    public $permissions;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Menu $menu) {
        $this->menu = $menu;
        $this->permissions = auth()->user()->permission ?? [];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render() {

        return view('template::components.menus.sidebar');
    }
}
