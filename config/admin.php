<?php

  return [

    /**
     * Url image logo
     */
    'logo' => env('ADMIN_LOGO', 'https://laravel.com/img/logotype.min.svg'),

    /**
     * Admin prefix page
     */
    'prefix' => env('ADMIN_PREFIX_PAGE', 'administrator'),

    /**
     * Admin account Model
     */
    'user' => App\Models\User::class,

    /**
     * Authentication Setting
     */
    'auth' => [
      /**
       * Set the guard to be used during authentication.
       */
      'guard' => 'web',

      /**
       * Set the broker to be used during password reset.
       */
      'broker' => 'users' // table name
    ],

    /**
     * Notification status
     */
    'notification' => env('ADMIN_NOTIFICATION', true),

    /**
     * Interval deleted log Activity
     */
    'log_activity_life' => 7, // Days

    /**
     * Option cached
     */

     'cache_option' => true

  ];
